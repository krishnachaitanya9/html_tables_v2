class HTML_Page():
    html_headings = """<!DOCTYPE html>
                            <html lang="en">
                            <head>
                              <title>Bootstrap Example</title>
                              

                              <style type="text/css">


                                    caption {
                                              background: #fff;
                                              padding: 5px;
                                              caption-side: top;
                                              font-size: 30px;
                                            }

                                  th, td {
                                      padding: 5px;
                                      text-align: center;
                                      
                                    }
                                    
                                th, td, table {
                                      border-collapse: collapse;
                                    }

                                  tr:hover {background-color: #e5e5e5;}

                                   

                              



                                </style>
                            </head>
                            <body>

    """

    html_ending = """

                                </body>
                                </html>                                             

        """



    def save_table(self, *args):
        open('test.html', 'w').close()
        with open('test.html', 'a') as htmlfile:
            htmlfile.write(self.html_headings + '\n'.join(arg.return_string() for arg in args) + self.html_ending)


class paragraph():
    def __init__(self, para_string):
        self.para_string = para_string

    def return_string(self):
        return '<div>'+self.para_string+'</div>'

class highest_stat_line():
    def __init__(self, unbold_line, bold_line):
        self.unbold_line = unbold_line
        self.bold_line = bold_line
    def return_string(self):
        return '<div>'+self.unbold_line+'<b>'+self.bold_line+'</b>'+'</div>'



class HTML_table():
    table_heading = """
                                <table class="table table-bordered table-hover" border="1">
                                    <caption class="text-center">{table_description}</caption>
                                      <thead class="thead-dark text-center lead">

                                          {add_headings}

                                      </thead>
                                      <tbody> 


    """
    table_columns = ""

    table_endings = """</tbody>
                        </table>"""

    def __init__(self, heading_list, table_description):
        self.table_heading = self.table_heading.format(table_description=table_description,
                                                       add_headings=self.get_heading_text(heading_list))

    def get_heading_text(self, column_list):
        column_tags = ""
        for every_element in column_list:
            htmltag = """<th>{element}</th>\n""".format(element=every_element)
            column_tags += htmltag

        final_table_text = column_tags
        return final_table_text

    def get_column_text(self, column_list):
        preceeding_text = """<tr class="text-center">"""
        succeeding_text = """</tr>"""
        column_tags = ""
        for every_element in column_list:
            htmltag = """<td>{element}</td>\n""".format(element=str(every_element))
            column_tags += htmltag

        final_table_text = preceeding_text + column_tags + succeeding_text
        return final_table_text

    def add_tuple_list(self, tuple_list):
        for each_tuple in tuple_list:
            self.add_column(list(each_tuple))

    def add_column(self, column_list):
        self.table_columns += self.get_column_text(column_list)

    def return_string(self):
        return self.table_heading + self.table_columns + self.table_endings